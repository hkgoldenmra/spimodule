#include <SPIModule.h>

void SPIModule::setPin(byte pin, bool data) {
	pinMode(pin, OUTPUT);
	digitalWrite(pin, data);
	delayMicroseconds(SPIModule::DELAY_MICROSECONDS);
}

bool SPIModule::getPin(byte pin) {
	pinMode(pin, INPUT);
	bool data = digitalRead(pin);
	delayMicroseconds(SPIModule::DELAY_MICROSECONDS);
	return data;
}

bool SPIModule::transferBit(bool data) {
	if (this->phase) {
		this->setPin(this->sckPin, !this->polarity);
		this->setPin(this->mosiPin, data);
		this->setPin(this->sckPin, this->polarity);
		return this->getPin(this->misoPin);
	} else {
		this->setPin(this->mosiPin, data);
		this->setPin(this->sckPin, !this->polarity);
		data = this->getPin(this->misoPin);
		this->setPin(this->sckPin, this->polarity);
		return data;
	}
}

byte SPIModule::transferByte(byte data) {
	for (byte shift = ((this->order) ? 0x80 : 0x01); shift > 0; ((this->order) ? shift >>= 1 : shift <<= 1)) {
		if (this->transferBit((data & shift) > 0)) {
			data &= (0xFF ^ shift) | shift;
		}
	}
	return data;
}

SPIModule::SPIModule(byte sckPin, byte mosiPin, byte misoPin, byte ssPin) {
	this->sckPin = sckPin;
	this->mosiPin = mosiPin;
	this->misoPin = misoPin;
	this->ssPin = ssPin;
}

SPIModule::SPIModule() :
SPIModule::SPIModule(SCK, MOSI, MISO, SS) {
}

void SPIModule::setOrder(bool order) {
	this->order = order;
}

void SPIModule::setPolarity(bool polarity) {
	this->polarity = polarity;
}

void SPIModule::setPhase(bool phase) {
	this->phase = phase;
}

void SPIModule::setMode(byte mode) {
	this->setPolarity((mode & 0x02) || (mode & 0x03));
	this->setPhase(mode & 0x01);
}

void SPIModule::initial() {
	this->setPin(this->ssPin, HIGH);
	this->setPin(this->sckPin, this->polarity);
}

void SPIModule::transfer(byte data[], byte length) {
	this->setPin(this->ssPin, LOW);
	for (byte i = 0; i < length; i++) {
		data[i] = this->transferByte(data[i]);
	}
	this->setPin(this->ssPin, HIGH);
}