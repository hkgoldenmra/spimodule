#ifndef SPIMODULE_H
#define SPIMODULE_H

#include <Arduino.h>

class SPIModule {
	private:
		static const unsigned int DELAY_MICROSECONDS = 50;
		byte sckPin;
		byte mosiPin;
		byte misoPin;
		byte ssPin;
		bool order;
		bool polarity;
		bool phase;
		void setPin(byte, bool);
		bool getPin(byte);
		bool transferBit(bool);
		byte transferByte(byte);
	public:
		static const bool ORDER_LSB = false;
		static const bool ORDER_MSB = true;
		static const bool POLARITY_LOW = false;
		static const bool POLARITY_HIGH = true;
		static const bool PHASE_START = false;
		static const bool PHASE_END = true;
		SPIModule(byte, byte, byte, byte);
		SPIModule();
		void setOrder(bool);
		void setPolarity(bool);
		void setPhase(bool);
		void setMode(byte);
		void initial();
		void transfer(byte[], byte);
};

#endif